<?php

namespace GROM\Models;

enum TrackParams: string
{
    case ARTIST = 'artist';
    case TITLE = 'title';
    case YEAR = 'year';
    case TRACK = 'num';
    case GENRE = 'genre';
    case COMMENT = 'comment';
    case ALBUM = 'album';
}
