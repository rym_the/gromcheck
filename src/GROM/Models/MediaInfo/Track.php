<?php

namespace GROM\Models\MediaInfo;

use GROM\Models\Tag\Album;
use GROM\Models\Tag\Artist;
use GROM\Models\Tag\Comment;
use GROM\Models\Tag\Genre;
use GROM\Models\Tag\Num;
use GROM\Models\Tag\Title;
use GROM\Models\Year;

class Track
{
    public function __construct(
        readonly Artist   $artist,
        readonly Title    $title,
        readonly ?Album    $album,
        readonly ?Year    $year,
        readonly ?Num     $num,
        readonly ?Genre   $genre,
        readonly ?Comment $comment
    )
    {}
}
