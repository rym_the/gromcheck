<?php

namespace GROM\Models\MediaInfo;

use GROM\Models\Tag\Album as AlbumTag;
use GROM\Models\Tag\Num;
use GROM\Models\Year;

class Album
{
    public function __construct(
        readonly iterable    $tracks,
        readonly ?AlbumTag $album,
        readonly ?Year $year,
        readonly ?Num $totalTracks
    )
    {}
}
