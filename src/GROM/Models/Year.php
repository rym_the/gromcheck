<?php

namespace GROM\Models;

use GROM\Models\Tag\TagValueInterface;

class Year implements TagValueInterface
{
    public function __construct(readonly int $year)
    {}

    public function getValue(): int
    {
        return $this->year;
    }
}
