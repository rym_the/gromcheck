<?php

namespace GROM\Models\Tag;

class Num implements TagValueInterface
{
    public function __construct(readonly int $year)
    {}

    public function getValue(): int
    {
        return $this->year;
    }
}
