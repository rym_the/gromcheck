<?php

namespace GROM\Models\Tag;

class Album implements TagValueInterface
{
    public function __construct(readonly string $album)
    {}

    public function getValue() : string
    {
        return $this->album;
    }
}
