<?php

namespace GROM\Models\Tag;

class Artist implements TagValueInterface
{
    public function __construct(readonly string $artist)
    {}

    public function getValue() : string
    {
        return $this->artist;
    }
}
