<?php

namespace GROM\Models\Tag;

interface TagValueInterface
{
    public function getValue(): mixed;
}
