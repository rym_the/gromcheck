<?php

namespace GROM\Models\Tag;

class Title implements TagValueInterface
{
    public function __construct(readonly string $title)
    {}

    public function getValue(): string
    {
        return $this->title;
    }
}
