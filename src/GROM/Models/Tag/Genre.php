<?php

namespace GROM\Models\Tag;

class Genre implements TagValueInterface
{
    public readonly string $genre;
    public function __construct(string $genre)
    {
        $this->genre = $genre;
    }

    public function getValue(): string
    {
         return $this->genre;
    }
}
