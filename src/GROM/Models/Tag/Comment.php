<?php

namespace GROM\Models\Tag;

class Comment implements TagValueInterface
{
    public function __construct(readonly string $comment)
    {}

    public function getValue(): string
    {
        return $this->comment;
    }
}
