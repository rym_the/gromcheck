<?php

namespace GROM\Models;

use GROM\Models\Tag\Album;
use GROM\Models\Tag\Artist;
use GROM\Models\Tag\Comment;
use GROM\Models\Tag\Genre;
use GROM\Models\Tag\Num;
use GROM\Models\Tag\TagValueInterface;
use GROM\Models\Tag\Title;
use InvalidArgumentException;

class ModelFactory
{
    public static function factory(TrackParams $trackParam, mixed $value): TagValueInterface
    {
        $className = static::getTagByParam($trackParam);
        return match ($className) {
            Artist::class, Comment::class, Genre::class, Title::class, Album::class => new $className((string) $value),
            Num::class, Year::class => new $className((int) $value),
            default => throw new InvalidArgumentException(sprintf('No tag found: %s', $className)),
        };
    }

    public static function getTagByParam(TrackParams $tag): string
    {
        return match ($tag) {
            TrackParams::ARTIST => Artist::class,
            TrackParams::ALBUM => Album::class,
            TrackParams::COMMENT => Comment::class,
            TrackParams::GENRE => Genre::class,
            TrackParams::TITLE => Title::class,
            TrackParams::TRACK => Num::class,
            TrackParams::YEAR => Year::class,
            default => throw new InvalidArgumentException(sprintf('Unknown tag parameter: %s', $tag->value)),
        };
    }

    public static function getParamsByTag(TagValueInterface $tag): TrackParams
    {
        return match ($tag::class) {
            Artist::class => TrackParams::ARTIST,
            Album::class => TrackParams::ALBUM,
            Comment::class => TrackParams::COMMENT,
            Genre::class => TrackParams::GENRE,
            Title::class => TrackParams::TITLE,
            Num::class => TrackParams::TRACK,
            Year::class => TrackParams::YEAR,
            default => throw new InvalidArgumentException(sprintf('Unknown tag: %s', $tag::class)),
        };
    }
}
