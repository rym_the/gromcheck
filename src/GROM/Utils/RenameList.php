<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */

namespace GROM\Utils;

use SplObjectStorage;

class RenameList extends SplObjectStorage
{
    /**
     * @param RenameFile $object
     * @return string
     */
    public function getHash($object): string
    {
        return $object->getOldName();
    }

    /**
     * @inheritDoc
     */
    public function attach($object, $data = null): void
    {
        if (!$object instanceof RenameFile) {
            return;
        }
        parent::attach($object, $data);
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($object, $data = null): void
    {
        if (!$data instanceof RenameFile) {
            return;
        }
        parent::offsetSet($object, $data);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($object): RenameFile
    {
        parent::offsetGet($object);
    }

    /**
     * @inheritDoc
     */
    public function current(): RenameFile
    {
        $object = parent::current();
        if (!$object instanceof RenameFile) {
            throw new \LogicException('Wrong object type');
        }
        return $object;
    }
}
