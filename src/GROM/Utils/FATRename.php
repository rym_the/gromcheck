<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */

namespace GROM\Utils;
/**
 * Class FATRename.
 * Helper to correct filenames and create short filenames for further usage.
 *
 */
class FATRename
{
    /**
     * Return valid name according to standard.
     * @param string $name
     * @return string Valid filename.
     */
    public static function getValidName($name)
    {
        return str_replace(array('\\', '/', ':', '*', '?', '"', '<', '>', '|'), '_', $name);
    }

    /**
     * Get short filename for provided long.
     * @param string $name
     * @return string
     */
    public static function getShortName($name)
    {
        $name = str_replace(' ', '', $name);
        $parts = explode('.', $name);
        $ext = array_pop($parts);
        $name = implode('', $parts);
        $name = str_replace(array(',', '[', ']', ';', '+', '='), '_', $name);
        $name = str_replace(' ', '', $name);
        if (strlen($name . '.' . $ext) > 8 || strlen($ext) > 3) {
            $name = substr($name, 0, 6) . '~1';
            $fname =   $name . '.' . substr($ext, 0, 3);
        } else {
            $fname = $name . '.' . $ext;
        }
        return strtoupper($fname);
    }

    /**
     * Replace cyrillic characters in provided name.
     * @param string $name
     * @return string
     */
    public static function getNonCyrillicValidName($name)
    {
        return preg_replace('/[^a-z0-9 .!#$%&\'\(\)\-@\^_`{}~]/imu', '_', static::getValidName($name));
    }
}
