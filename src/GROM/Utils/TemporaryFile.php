<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */

namespace GROM\Utils;

use \Exception;
use Stringable;

class TemporaryFile implements Stringable
{
    /**
     * @var string
     */
    private $tmpDir;

    /**
     * @var string
     */
    private $original;

    /**
     * @var string|null
     */
    private $tmpFile;

    private CommandInterface $syncTool;

    /**
     * TemporaryFile constructor.
     * @param string $path
     * @param string $syncTool
     * @param string|null $tmpDir
     */
    public function __construct(string $path, CommandInterface $syncTool, ?string $tmpDir = null)
    {
        $this->tmpDir = rtrim($tmpDir ?? sys_get_temp_dir(), DIRECTORY_SEPARATOR);
        $this->syncTool = $syncTool;
        $this->original = $path;
    }

    /**
     * Destructor to be sure temp file deleted.
     */
    public function __destruct()
    {
        $this->removeTmpFile();
    }

    /**
     * Copy original file to temporary directory
     */
    public function copy(): void
    {
        $fInfo = new \SplFileInfo($this->original);
        $filename = $fInfo->getFilename();
        $this->tmpFile = sprintf("%s/%s", $this->tmpDir, $filename);
        $this->syncTool->execute(null, ['i' => $this->original, 'o' => $this->tmpFile], []);
    }

    /**
     * Copy temp file to original path.
     * @throws Exception
     */
    public function restore(): void
    {
        if (!$this->tmpFile) {
            throw new Exception('No temporary file exists');
        }
        $this->syncTool->execute(null, ['o' => $this->original, 'i' => $this->tmpFile], []);
        $this->removeTmpFile();
    }

    /**
     * Delete temporary file.
     */
    private function removeTmpFile(): void
    {
        if (!$this->tmpFile) {
            return;
        }
        unlink($this->tmpFile);
        $this->tmpFile = null;
    }

    public function __toString(): string
    {
        return $this->tmpFile;
    }
}
