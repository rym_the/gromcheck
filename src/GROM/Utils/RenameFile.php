<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */

namespace GROM\Utils;

use Exception;

class RenameFile
{
    /**
     * @var string
     */
    private $oldName;

    /**
     * @var string
     */
    private $newName;

    /**
     * RenameFile constructor.
     * @param string $oldName
     * @param string $newName
     * @throws Exception
     */
    public function __construct(string $oldName, string $newName)
    {
        if ($oldName === $newName) {
            throw new Exception('Nothing to rename');
        }
        $this->newName = $newName;
        $this->oldName = $oldName;
    }

    /**
     * @return string
     */
    public function getOldName(): string
    {
        return $this->oldName;
    }

    /**
     * @return string
     */
    public function getNewName(): string
    {
        return $this->newName;
    }
}
