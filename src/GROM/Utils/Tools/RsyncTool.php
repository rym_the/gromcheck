<?php

namespace GROM\Utils\Tools;

use GROM\Utils\CommandInterface;

class RsyncTool implements CommandInterface
{
    public function __construct(readonly string $executable)
    {
        if (!is_file($this->executable) && !is_executable($this->executable)) {
            throw new \RuntimeException('Executable not found');
        }
    }

    public function execute(?string $path, ?array $shortOpts, ?array $longOpts): array
    {
        $shortOpts ??= [];
        $inputFile = $shortOpts['i'] ?? null;
        $outputFile = $shortOpts['o'] ?? null;
        if ($inputFile === null || $outputFile === null) {
            throw new \InvalidArgumentException('Wrong params for rsync');
        }
        $command = sprintf(
            '%s %s %s',
            $this->executable,
            escapeshellarg($inputFile),
            escapeshellarg($outputFile)
        );
        exec($command, $output);
        return $output;
    }
}
