<?php

namespace GROM\Utils\Tools;

use GROM\Models\MediaInfo\Track;
use GROM\Models\ModelFactory;
use GROM\Models\TrackParams;
use GROM\Utils\CommandInterface;
use GROM\Utils\Translit;

class TrackInfoFlac implements TrackInfoInterface
{
    private const METAFLAC_TAGS = [
        'ALBUM' => TrackParams::ALBUM,
        'ARTIST' => TrackParams::ARTIST,
        'ALBUMARTIST' => TrackParams::ARTIST,
        'TITLE' => TrackParams::TITLE,
        'DATE' => TrackParams::YEAR,
        'TRACKNUMBER' => TrackParams::TRACK,
        'GENRE' => TrackParams::GENRE,
        'COMMENTS' => TrackParams::COMMENT,
    ];

    public function __construct(readonly CommandInterface $metaflac, readonly ModelFactory $modelFactory)
    {
    }

    public function setInfo(string $file, Track $info): void
    {
        $this->metaflac->execute($file, [], ['remove-all' => null]);
        $this->metaflac->execute(
            $file,
            [],
            ['remove' => null, 'block-type' => 'PICTURE,PADDING', 'dont-use-padding' => null]
        );
        $this->metaflac->execute(
            $file,
            [],
            ['remove-tag' => 'COVERART', 'dont-use-padding' => null]
        );
        $metadata = [];
        foreach ([$info->artist, $info->title, $info->year, $info->genre, $info->comment, $info->num, $info->album] as $tag) {
            if (!$tag) {
                continue;
            }
            $tagParam = $this->modelFactory->getParamsByTag($tag);
            $tagKey = array_search($tagParam, static::METAFLAC_TAGS, true);
            if ($tagKey !== false) {
                $metadata[] = sprintf(
                    '%s=%s',
                    $tagKey,
                    Translit::translit($tag->getValue())
                );
            }
        }
        $this->metaflac->execute(
            $file,
            [],
            ['add-seekpoint' => '10s', 'set-tag' => $metadata]
        );
    }

    public function getInfo(string $file): Track
    {

        $output = $this->metaflac->execute(
            $file,
            [],
            ['show-tag' => array_keys(static::METAFLAC_TAGS)]
        );
        $trackParams = [];
        foreach ($output as $tag) {
            $parts = explode('=', $tag);
            $key = strtoupper($parts[0]);
            $origKey = self::METAFLAC_TAGS[$key] ?? null;
            if ($origKey !== null) {
                $trackParams[$origKey->value] = $this->modelFactory->factory($origKey, $parts[1]);
            }
        }
        return new Track(
            $trackParams[TrackParams::ARTIST->value] ?? null,
            $trackParams[TrackParams::TITLE->value] ?? null,
            $trackParams[TrackParams::ALBUM->value] ?? null,
            $trackParams[TrackParams::YEAR->value] ?? null,
            $trackParams[TrackParams::TRACK->value] ?? null,
            $trackParams[TrackParams::GENRE->value] ?? null,
            $trackParams[TrackParams::COMMENT->value] ?? null,
        );
    }
}