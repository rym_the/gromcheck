<?php

namespace GROM\Utils\Tools;

use GROM\Models\MediaInfo\Track;

interface TrackInfoInterface
{
    public function getInfo(string $file): Track;

    public function setInfo(string $file, Track $info): void;
}
