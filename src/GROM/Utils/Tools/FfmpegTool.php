<?php

namespace GROM\Utils\Tools;

use GROM\Utils\CommandInterface;

class FfmpegTool implements CommandInterface
{
    public function __construct(readonly string $executable)
    {
        if (!is_file($this->executable) && !is_executable($this->executable)) {
            throw new \RuntimeException('Executable not found');
        }
    }

    public function execute(?string $path, ?array $shortOpts, ?array $longOpts): array
    {
        $output = [];
        $command = $path ? sprintf('%s %s', $this->executable, escapeshellarg($path)) : $this->executable;
        $outputFile = null;
        if (isset($shortOpts['o'])) {
            $outputFile = escapeshellarg($shortOpts['o']);
            unset($shortOpts['o']);
        }
        foreach ($shortOpts ?? [] as $paramKey => $param) {
            if ($param !== null) {
                if (is_array($param)) {
                    foreach ($param as $subParam) {
                        $command = sprintf(
                            '%s -%s %s',
                            $command,
                            $paramKey,
                            escapeshellarg($subParam)
                        );
                    }
                } else {
                    $command = sprintf(
                        '%s -%s %s',
                        $command,
                        $paramKey,
                        escapeshellarg($param)
                    );
                }
            } else {
                $command = sprintf(
                    '%s -%s',
                    $command,
                    $paramKey
                );
            }
        }

        foreach ($longOpts ?? [] as $paramKey => $param) {
            if ($param !== null) {
                if (is_array($param)) {
                    foreach ($param as $subParam) {
                        $command = sprintf(
                            '%s --%s %s',
                            $command,
                            $paramKey,
                            escapeshellarg($subParam)
                        );
                    }
                } else {
                    $command = sprintf(
                        '%s --%s %s',
                        $command,
                        $paramKey,
                        escapeshellarg($param)
                    );
                }
            } else {
                $command = sprintf(
                    '%s --%s',
                    $command,
                    $paramKey
                );
            }
        }
        if ($outputFile) {
            $command = sprintf('%s %s', $command, $outputFile);
        }

        exec($command, $output);
        return $output;
    }
}
