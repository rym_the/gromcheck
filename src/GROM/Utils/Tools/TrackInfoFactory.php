<?php

namespace GROM\Utils\Tools;

use Psr\Container\ContainerInterface;
use RuntimeException;

class TrackInfoFactory
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function getMediaInfo(string $file): TrackInfoInterface
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        $trackInfo = match (strtolower($ext)) {
            'flac' => $this->container->get(TrackInfoFlac::class),
            'mp3' => $this->container->get(TrackInfoMp3::class),
            'm4a', 'ogg', 'aac', 'wav' => null,
            default => null,
        };
        if (!$trackInfo instanceof TrackInfoInterface) {
            throw new RuntimeException('Format not supported');
        }
        return $trackInfo;
    }
}
