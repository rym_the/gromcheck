<?php

namespace GROM\Utils\Tools;

use GROM\Models\MediaInfo\Track;
use GROM\Models\ModelFactory;
use GROM\Models\Tag\TagValueInterface;
use GROM\Models\TrackParams;
use GROM\Utils\CommandInterface;
use GROM\Utils\TemporaryFile;
use GROM\Utils\Translit;

class TrackInfoMp3 implements TrackInfoInterface
{
    private const FFPROBE_TAGS = [
        'TAG:album' => TrackParams::ALBUM,
        'TAG:artist' => TrackParams::ARTIST,
        'TAG:album_artist' => TrackParams::ARTIST,
        'TAG:title' => TrackParams::TITLE,
        'TAG:date' => TrackParams::YEAR,
        'TAG:track' => TrackParams::TRACK,
        'TAG:genre' => TrackParams::GENRE,
        'TAG:comment' => TrackParams::COMMENT,
    ];
    private const FFMPEG_CLASS_TAGS = [
        'album' => TrackParams::ALBUM,
        'artist' => TrackParams::ARTIST,
        'title' => TrackParams::TITLE,
        'year' => TrackParams::YEAR,
        'track' => TrackParams::TRACK,
        'genre' => TrackParams::GENRE,
        'comment' =>TrackParams::COMMENT,
    ];
    public function __construct(
        readonly CommandInterface $ffprobe,
        readonly CommandInterface $ffmpeg,
        readonly CommandInterface $rsync,
        readonly ModelFactory $modelFactory
    ) {}

    public function getInfo(string $file): Track
    {
        $trackParams = [];
        $ffprobeout = $this->ffprobe->execute(null, ['i' => $file, 'loglevel' => '16', 'show_format' => null,], []);

        $matches = [];
        $write = 0;
        foreach ($ffprobeout as $output) {
            if ($write === 1)  {
                if ($output === '[/FORMAT]') {
                    $write = 0;
                } else {
                    $matches[] = $output;
                }
            }
            if ($write === 0 && $output === '[FORMAT]') {
                $write = 1;
            }
        }
        foreach ($matches as $out) {
            $parts = explode('=', $out, 2);
            $key = $parts[0];
            $origKey = self::FFPROBE_TAGS[$key] ?? null;
            if ($origKey !== null) {
                $trackParams[$origKey->value] = $this->modelFactory->factory($origKey, $parts[1]);
            }
        }
        return new Track(
            $trackParams[TrackParams::ARTIST->value] ?? null,
            $trackParams[TrackParams::TITLE->value] ?? null,
            $trackParams[TrackParams::ALBUM->value] ?? null,
            $trackParams[TrackParams::YEAR->value] ?? null,
            $trackParams[TrackParams::TRACK->value] ?? null,
            $trackParams[TrackParams::GENRE->value] ?? null,
            $trackParams[TrackParams::COMMENT->value] ?? null,
        );
    }

    public function setInfo(string $file, Track $info): void
    {
        /**
         * `-y` always Yes
         * `-v 16` show only errors
         * `-map_metadata -1` clear all (`-map 0:a`)
         * `-write_id3v1 0 -write_id3v2 1` write only id3v2
         * `-id3v2_version 3` id3v2.3
         * `-f mp3` mp3 format
         * `-b:a 320k` CBR 320k
         * `-codec:a copy` copy audio track - can be used `-codec:a mp3` to recode
         */
        $tmpFile = new TemporaryFile($file, $this->rsync);
        $tmpFile->copy();
        $metadata = [];
        /** @var TagValueInterface $tag */
        foreach ([$info->artist, $info->title, $info->year, $info->genre, $info->comment, $info->num, $info->album] as $tag) {
            if (!$tag) {
                continue;
            }
            $tagParam = $this->modelFactory->getParamsByTag($tag);
            $tagKey = array_search($tagParam, static::FFMPEG_CLASS_TAGS, true);
            if ($tagKey !== false) {
                $metadata[] = sprintf(
                    '%s=%s',
                    $tagKey,
                    Translit::translit($tag->getValue())
                );
            }
        }
        $opts = [
            'y' => null,
            'v' => 16,
            'i' => $file,
            'map_metadata' => '-1',
            'write_id3v1' => 0,
            'write_id3v2' => 1,
            'write_xing' => 1,
            'id3v2_version' => 3,
            'f' => 'mp3',
            'b:a' => '320k',
            'c:a' => 'copy',
            'vn' => null,
            'sn' => null,
            'metadata' => $metadata,
            'o' => $tmpFile,
        ];
        $this->ffmpeg->execute(null, $opts, []);
        $tmpFile->restore();
    }
}
