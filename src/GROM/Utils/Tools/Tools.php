<?php

namespace GROM\Utils\Tools;

enum Tools: string
{
    case FFPROBE = 'ffprobe';
    case FFMPEG = 'ffmpeg';
    case METAFLAC = 'metaflac';
    case RSYNC = 'rsync';
}
