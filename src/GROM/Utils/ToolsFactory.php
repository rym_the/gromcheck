<?php

namespace GROM\Utils;

use Exception;
use GROM\Utils\Tools\FfmpegTool;
use GROM\Utils\Tools\FfprobeTool;
use GROM\Utils\Tools\MetaFlacTool;
use GROM\Utils\Tools\RsyncTool;
use GROM\Utils\Tools\Tools;
use WeakMap;

class ToolsFactory
{
    private static WeakMap $tools;

    public static function getTool(Tools $tool): CommandInterface
    {
        static::$tools ??= new WeakMap();
        $s = $tool->value;
        foreach (static::$tools as $toolEx => $name) {
            if ($name === $s) {
                return $toolEx;
            }
        }
        $command = "command -v {$s}";
        $out = array();
        exec($command, $out);
        $out = explode(' ', trim(str_replace(sprintf('%s:', $s), '', array_shift($out))));
        $commandPath = trim(array_shift($out));
        if (empty($command)) {
            throw new Exception("Can't find {$s}");
        }
        $command = match ($tool) {
            Tools::METAFLAC => new MetaFlacTool($commandPath),
            Tools::FFPROBE => new FfprobeTool($commandPath),
            Tools::FFMPEG => new FfmpegTool($commandPath),
            Tools::RSYNC => new RsyncTool($commandPath),
            default => new Command($commandPath),
        };
        self::$tools[$command] = $s;
        return $command;
    }
}
