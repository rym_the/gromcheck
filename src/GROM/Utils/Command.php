<?php

namespace GROM\Utils;

class Command implements CommandInterface
{
    public function __construct(readonly string $executable)
    {
        if (!is_file($this->executable) && !is_executable($this->executable)) {
            throw new \RuntimeException('Executable not found');
        }
    }

    public function execute(?string $path, ?array $shortOpts, ?array $longOpts): array
    {
        $output = [];
        $command = $path ? sprintf('%s %s', $this->executable, $path) : $this->executable;
        foreach ($shortOpts ?? [] as $paramKey => $param) {
            if ($param !== null) {
                if (is_array($param)) {
                    foreach ($param as $subParam) {
                        $command = sprintf(
                            '%s -%s %s',
                            $command,
                            $paramKey,
                            escapeshellcmd($subParam)
                        );
                    }
                } else {
                    $command = sprintf(
                        '%s -%s %s',
                        $command,
                        $paramKey,
                        escapeshellcmd($param)
                    );
                }
            } else {
                $command = sprintf(
                    '%s -%s',
                    $command,
                    $paramKey
                );
            }
        }

        foreach ($longOpts ?? [] as $paramKey => $param) {
            if ($param !== null) {
                if (is_array($param)) {
                    foreach ($param as $subParam) {
                        $command = sprintf(
                            '%s --%s %s',
                            $command,
                            $paramKey,
                            escapeshellcmd($subParam)
                        );
                    }
                } else {
                    $command = sprintf(
                        '%s --%s %s',
                        $command,
                        $paramKey,
                        escapeshellcmd($param)
                    );
                }
            } else {
                $command = sprintf(
                    '%s --%s',
                    $command,
                    $paramKey
                );
            }
        }

        exec($command, $output);
        return $output;
    }
}
