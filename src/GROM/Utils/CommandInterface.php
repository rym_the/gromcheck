<?php

namespace GROM\Utils;

interface CommandInterface
{
    public function execute(?string $path, ?array $shortShortOpts, ?array $longOpts): array;
}
