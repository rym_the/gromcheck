<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */

namespace GROM\Utils;

/**
 * Class YAFSSorter
 * Sort order of files in YAFS output file.
 * @package GROM
 */
class YAFSSorter
{
    /**
     * Return destination filename.
     */
    public function getDestination(string $source): string
    {
        $tmp = explode('.', $source);
        $ext = array_pop($tmp);
        array_push($tmp, 'new', $ext);
        return implode('.', $tmp);
    }

    /**
     * Run sorting of elements in YAFS xml file.
     */
    public function run(string $source): void
    {
        if (!file_exists($source) || !is_readable($source)) {
            throw new \Exception('File should be readable');
        }

        $fi = simplexml_load_file($source,
            'SimpleXMLElement',
            LIBXML_DTDLOAD & LIBXML_NOBLANKS & LIBXML_PARSEHUGE
        );

        $cnt = 1;
        foreach ($fi->directory as $rootDir) {
            $this->setElementOrder($rootDir);
            $cnt++;
            foreach ($rootDir->directory as $path) {
                $this->setElementOrder($path);
                foreach ($path->file as $file) {
                    $this->setElementOrder($file);
                }
            }
        }
        foreach ($fi->file as $file) {
            $this->setElementOrder($file, $cnt++);
        }
        $fi->saveXML($this->getDestination($source));
    }

    /**
     * Change order  attribute of an element according to it's name.
     * @param \SimpleXMLElement $element Element to change order for.
     * @param int $default Default order.
     */
    protected function setElementOrder(\SimpleXMLElement $element, $default = 1)
    {
        $name = $element->long_name->__toString();
        if (!$name) {
            $name = $element->short_name;
        }
        $order = substr($name, 0, 2);
        if (empty($order) || !is_numeric($order)) {
            $order = sprintf('%1$02d', $default);
        }
        $element['order'] = $order;
    }
}
