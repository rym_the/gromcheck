<?php

namespace GROM\Utils;

class Translit
{
     private const TRANSLIT_MAP = [
        'а' => 'a',
        'б' => 'b',
        'в' => 'v',
        'г' => 'g',
        'д' => 'd',
        'е' => 'e',
        'ё' => 'e',
        'з' => 'z',
        'и' => 'i',
        'й' => 'i',
        'к' => 'k',
        'л' => 'l',
        'м' => 'm',
        'н' => 'n',
        'о' => 'o',
        'п' => 'p',
        'р' => 'r',
        'с' => 's',
        'т' => 't',
        'у' => 'u',
        'ф' => 'f',
        'х' => 'x',
        'ъ' => '_',
        'ы' => '_',
        'э' => '_',
        'А' => 'A',
        'Б' => 'B',
        'В' => 'V',
        'Г' => 'G',
        'Д' => 'D',
        'Е' => 'E',
        'Ё' => 'E',
        'З' => 'Z',
        'И' => 'I',
        'Й' => 'I',
        'К' => 'K',
        'Л' => 'L',
        'М' => 'M',
        'Н' => 'N',
        'О' => 'O',
        'П' => 'P',
        'Р' => 'R',
        'С' => 'S',
        'Т' => 'T',
        'У' => 'U',
        'Ф' => 'F',
        'Х' => 'X',
        'Ъ' => '_',
        'Ы' => '_',
        'Э' => '_',
        'ж' => 'zh',
        'ц' => 'ts',
        'ч' => 'ch',
        'ш' => 'sh',
        'щ' => 'shch',
        'ь' => '_',
        'ю' => 'yu',
        'я' => 'ya',
        'Ж' => 'Zh',
        'Ц' => 'Ts',
        'Ч' => 'Ch',
        'Ш' => 'Sh',
        'Щ' => 'Shch',
        'Ь' => '_',
        'Ю' => 'Yu',
        'Я' => 'Ya',
    ];

    static function translit(mixed $input): mixed
    {
        if (!is_string($input)) {
            return $input;
        }
        return str_replace(array_keys(static::TRANSLIT_MAP), array_values(static::TRANSLIT_MAP), $input);
    }
}