<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */
namespace GROM\Commands;

use Exception;
use FilesystemIterator;
use GROM\Models\MediaInfo\Album;
use GROM\Models\MediaInfo\Track;
use GROM\Models\Tag\Num;
use GROM\Utils\Tools\TrackInfoFactory;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;
use WeakMap;

/**
 * Rename, Check integrity for media files.
 * Class Rename
 */
class UpdateTag extends Command
{
    protected static $defaultName = 'updatetag';
    protected static $defaultDescription = 'Update tags of files';
    protected static $defaultHelp = '--directory Directory to work with';

    private TrackInfoFactory $trackInfoFactory;

    public function __construct(TrackInfoFactory $trackInfoFactory)
    {
        parent::__construct(null);
        $this->trackInfoFactory = $trackInfoFactory;
    }

    public function checkSource($path)
    {
        if (empty($path)) {
            throw new Exception('Empty source path');
        }
        if (!file_exists($path)) {
            throw new Exception("{$path} is not exist");
        }
        $fInfo = new SplFileInfo($path);

        if (!$fInfo->isDir()) {
            throw new Exception("{$path} is not a directory");
        }
        if (!$fInfo->isReadable() || !$fInfo->isWritable()) {
            throw new Exception("{$path} is not assessable");
        }
        return rtrim($fInfo->getRealPath(), DIRECTORY_SEPARATOR);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = $input->getArgument('directory');
        $output->writeln(
            sprintf('Start update tag on %s', $directory),
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        try {
            $source = $this->checkSource($directory);
            $this->checkNonRootPath($source);
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage(), OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL);
            return Command::FAILURE;
        }
        $output->writeln(
            'End update tag',
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        return Command::SUCCESS;
    }

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
        $this->setDescription(self::$defaultDescription);
        $this->setHelp(self::$defaultHelp);
        $this->addArgument('directory', InputArgument::REQUIRED, 'Directory to work with');
    }

    /**
     * Return iterator for path.
     * @param string $path
     * @return FilesystemIterator
     *
     * @throws UnexpectedValueException
     */
    private function getIterator(string $path): FilesystemIterator
    {
        return new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS|FilesystemIterator::NEW_CURRENT_AND_KEY);
    }


    /**
     * Check 1st level path.
     * @param string $root
     * @throws Exception
     */
    protected function checkNonRootPath(string $root)
    {
        $iterator = $this->getIterator($root);

        foreach ($iterator as $path) {
            if (!$path->isDir()) {
                continue;
            }
            $categoryIterator = $this->getIterator($path->getRealPath());
            foreach ($categoryIterator as $category) {
                if (!$category->isDir()) {
                    continue;
                }
                $this->updateTagsInPath($category);
            }
        }
    }

    /**
     * Check files in 2nd level directories.
     * @param SplFileInfo $path
     * @throws Exception
     */
    protected function updateTagsInPath(SplFileInfo $path): Album
    {
        $tracks = new WeakMap();
        $year = null;
        $albumTitle = null;

        $iterator = $this->getIterator($path->getRealPath());

        foreach ($iterator as $file) {
            if (!$file->isFile()) {
                continue;
            }
            try {
                $trackInfo = $this->getMediaInfo($file);
            } catch (\Throwable $exception) {
                continue;
            }
            $year ??= $trackInfo->year;
            $albumTitle ??= $trackInfo->album;
            $this->setMediaInfo($file, $trackInfo);
            $tracks[$file] = $trackInfo;
        }
        return new Album($tracks, $albumTitle, $year, new Num(count($tracks)));
    }

    private function getMediaInfo($file): Track
    {
        $trackInfo = $this->trackInfoFactory->getMediaInfo($file);
        return $trackInfo->getInfo($file);
    }

    private function setMediaInfo($file, Track $track): void
    {
        $trackInfo = $this->trackInfoFactory->getMediaInfo($file);
        $trackInfo->setInfo($file, $track);
    }
}
