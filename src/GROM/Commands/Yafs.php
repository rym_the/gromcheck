<?php

namespace GROM\Commands;

use Exception;
use GROM\Utils\YAFSSorter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Yafs extends Command
{
    protected static $defaultName = 'yafs';
    protected static $defaultDescription = 'Sort YAFS source file to GROM Audio';
    protected static $defaultHelp = '--xml XML to work with';
    /**
     * @var YAFSSorter
     */
    private $sorter;

    public function __construct(YAFSSorter $sorter)
    {
        parent::__construct();
        $this->sorter = $sorter;
    }

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
        $this->setDescription(self::$defaultDescription);
        $this->setHelp(self::$defaultHelp);
        $this->addArgument('xml', InputArgument::REQUIRED, 'XML to work with');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $xml = $input->getArgument('xml');
        try {
            $this->sorter->run($xml);
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage(), OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL);
            return Command::FAILURE;
        }
        return Command::SUCCESS;
    }
}
