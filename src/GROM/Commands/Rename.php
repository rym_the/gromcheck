<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */
namespace GROM\Commands;

use Exception;
use FilesystemIterator;
use GROM\Utils\FATRename;
use GROM\Utils\RenameFile;
use GROM\Utils\RenameList;
use GROM\Utils\Translit;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;

/**
 * Rename, Check integrity for media files.
 * Class Rename
 */
class Rename extends Command
{
    protected static $defaultName = 'rename';
    protected static $defaultDescription = 'Clean tags in files and rename files';
    protected static $defaultHelp = '--directory Directory to work with';

    /**
     * Regexp to work with directories.
     * @var string
     */
    protected $directoryMatch = '/^(\d{1,4})[ \.\-_]{0,3}/i';

    public function checkSource($path)
    {
        if (empty($path)) {
            throw new Exception('Empty source path');
        }
        if (!file_exists($path)) {
            throw new Exception("{$path} is not exist");
        }
        $fInfo = new SplFileInfo($path);

        if (!$fInfo->isDir()) {
            throw new Exception("{$path} is not a directory");
        }
        if (!$fInfo->isReadable() || !$fInfo->isWritable()) {
            throw new Exception("{$path} is not assessable");
        }
        return rtrim($fInfo->getRealPath(), DIRECTORY_SEPARATOR);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = $input->getArgument('directory');
        $output->writeln(
            sprintf('Start rename on %s', $directory),
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        try {
            $source = $this->checkSource($directory);
            $this->checkRootPath($source);
            $this->checkNonRootPath($source);
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage(), OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL);
            return Command::FAILURE;
        }
        $output->writeln(
            'End rename',
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        return Command::SUCCESS;
    }

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
        $this->setDescription(self::$defaultDescription);
        $this->setHelp(self::$defaultHelp);
        $this->addArgument('directory', InputArgument::REQUIRED, 'Directory to work with');
    }

    /**
     * Return iterator for path.
     * @param string $path
     * @return FilesystemIterator
     *
     * @throws UnexpectedValueException
     */
    private function getIterator(string $path): FilesystemIterator
    {
        return new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS|FilesystemIterator::NEW_CURRENT_AND_KEY);
    }

    /**
     * Check root path for correct folder number and check valid names.
     * @param string $path Path to work with.
     *
     * @throws Exception
     */
    protected function checkRootPath($path)
    {
        $directories = [];
        $iterator = $this->getIterator($path);

        foreach ($iterator as $file) {
            if ($file->isDir()) {
                array_push($directories, $file);
            }
        }

        $renameList = $this->getRenameList($directories);
        $this->renamePaths($renameList);
    }

    /**
     * Rename provided path.
     * @param RenameList $list Paths to rename.
     * @return int Count of renamed paths.
     */
    protected function renamePaths(RenameList $list): int
    {
        $count = 0;
        foreach ($list as $rename) {
            if (rename($rename->getOldName(), $rename->getNewName())) {
                $count++;
            } else {
                throw new Exception('Cant rename' . $rename->getOldName());
            }
        }
        return $count;
    }

    /**
     * Checks whether we need to rename provided names.
     * Also make a transliterate for provided names.
     * @param SplFileInfo[] $names Names to work with.
     * @return RenameList Converted names
     * @throws Exception
     */
    protected function getRenameList(array $names): RenameList
    {
        if (false === sort($names, SORT_NATURAL)) {
            throw new Exception('Can\'t sort names');
        };

        $renameList = new RenameList();
        $matches = array();
        $counter = 0;

        foreach ($names as $path) {
            $oldName = $path->getFilename();
            $counter++;
            if (preg_match($this->directoryMatch, $oldName, $matches)) {
                $newName = sprintf('%1$02d.%2$s', $counter, substr($oldName, strlen($matches[0])));
            } else {
                $newName = sprintf('%1$02d.%2$s', $counter, $oldName);
            }

            $newName = $this->translit($newName);
            $newName = FATRename::getNonCyrillicValidName($newName);
            $newName = str_replace($oldName, $newName, $path->getRealPath());
            try {
                $rename = new RenameFile($path->getRealPath(), $newName);
                $renameList->attach($rename);
            } catch (Exception $exception) {
                //do nothing
            }
        }
        return $renameList;
    }

    /**
     * Check 1st level path.
     * @param string $root
     * @throws Exception
     */
    protected function checkNonRootPath(string $root)
    {
        $iterator = $this->getIterator($root);

        foreach ($iterator as $file) {
            if ($file->isDir()) {
                $this->checkPath($file);
            }
        }
    }

    /**
     * Check 2nd level path.
     * @param SplFileInfo $path
     * @throws Exception
     */
    protected function checkPath(SplFileInfo $path): void
    {
        $directories = [];

        $iterator = $this->getIterator($path->getRealPath());

        foreach ($iterator as $file) {
            if ($file->isDir()) {
                array_push($directories, $file);
                $this->checkFiles($file);
            }
        }

        $renameList = $this->getRenameList($directories);
        $this->renamePaths($renameList);
    }

    /**
     * Check files in 2nd level directories.
     * @param SplFileInfo $path
     * @throws Exception
     */
    protected function checkFiles(SplFileInfo $path)
    {
        $files = [];

        $iterator = $this->getIterator($path->getRealPath());

        foreach ($iterator as $file) {
            if ($file->isFile()) {
                array_push($files, $file);
            }
        }

        $renameList = $this->getRenameList($files);
        $this->renamePaths($renameList);
    }

    /**
     * Transliterate input string.
     * @param $in
     * @return mixed
     */
    protected function translit($in)
    {
        return Translit::translit($in);
    }
}
