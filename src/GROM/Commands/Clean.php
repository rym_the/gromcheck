<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */
namespace GROM\Commands;

use DirectoryIterator;
use Exception;
use FilesystemIterator;
use SplFileInfo;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UnexpectedValueException;

/**
 * Remove useless files from path.
 * Class Cleaner
 * @package GROM
 */
class Clean extends Command
{
    protected static $defaultName = 'cleaner';
    protected static $defaultDescription = 'Clean directories and files';
    protected static $defaultHelp = '--directory Directory to work with';

    private const DIRECTORY_LIMIT = 5;

    /**
     * Suitable extensions for files.
     * @var array
     */
    protected $exts = [
        'flac',
        'mp3',
        'm4a',
        'ogg',
        'aac',
        'wav',
    ];

    /**
     * Hash of files to remove.
     * @var SplFileInfo[]
     */
    protected $filesToRemove = [];

    /**
     * @var SplFileInfo[]
     */
    protected $suitableFiles = [];

    protected function configure() : void
    {
        $this->setName(self::$defaultName);
        $this->setDescription(self::$defaultDescription);
        $this->setHelp(self::$defaultHelp);
        $this->addArgument('directory', InputArgument::REQUIRED, 'Directory to work with');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $directory = $input->getArgument('directory');
        $output->writeln(
            sprintf('Start cleaning on %s', $directory),
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        try {
            $source = $this->checkSource($directory);
            $this->checkRoot($source);
            $this->removeFiles($this->filesToRemove);
        } catch (Exception $exception) {
            $output->writeln($exception->getMessage(), OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL);
            return Command::FAILURE;
        }
        $output->writeln(
            'End cleaning',
            OutputInterface::VERBOSITY_QUIET | OutputInterface::OUTPUT_NORMAL
        );
        return Command::SUCCESS;
    }

    /**
     * Remove useless files.
     * @param SplFileInfo[] $files Files to remove.
     * @throws Exception
     */
    protected function removeFiles(array $files)
    {
        if (false === sort($files, SORT_NATURAL)) {
            throw new Exception('Can\'t sort directories');
        }

        foreach ($files as $file) {
            if (!file_exists($file->getRealPath())) {
                continue;
            }
            $this->removeDir($file);
        }
    }

    /**
     * Remove single directory.
     * @param SplFileInfo $dir
     * @throws Exception
     */
    private function removeDir(SplFileInfo $dir): void
    {
        if (!$dir->isWritable()) {
            throw new Exception("{$dir->getRealPath()} is not writable");
        }
        if (!$dir->isDir()) {
            $this->removeFile($dir);
            return;
        }
        $iterator = $this->getIterator($dir->getRealPath());

        /**
         * @var DirectoryIterator $file
         */
        foreach ($iterator as $file) {
            if ($file->isFile()) {
                $this->removeFile($file);
            }
            if ($file->isDir()) {
                $this->removeDir($file);
            }
        }
        rmdir($dir->getRealPath());
    }

    /**
     * Remove single file.
     * @param SplFileInfo $file
     */
    public function removeFile(SplFileInfo $file): void
    {
        unlink($file->getRealPath());
    }

    /**
     * Check source path to work with.
     * @throws Exception
     */
    public function checkSource(string $path): string
    {
        if (empty($path)) {
            throw new Exception('Empty source path');
        }
        if (!file_exists($path)) {
            throw new Exception("{$path} is not exist");
        }
        $fInfo = new SplFileInfo($path);

        if (!$fInfo->isDir()) {
            throw new Exception("{$path} is not a directory");
        }
        if (!$fInfo->isReadable() || !$fInfo->isWritable()) {
            throw new Exception("{$path} is not assessable");
        }
        return rtrim($fInfo->getRealPath(), DIRECTORY_SEPARATOR);
    }

    /**
     * Check 1st level path.
     * @param string $path Path to work with.
     * @throws Exception
     */
    protected function checkRoot(string $path): void
    {
        $directoryCount = 0;
        $iterator = $this->getIterator($path);

        foreach ($iterator as $file) {
            if ($file->isFile() || $file->isLink()) {
                $this->markForDelete($file);
            }
            if ($file->isDir()) {
                if (!$this->checkPath($file)) {
                    $this->markForDelete($file);
                } else {
                    $directoryCount ++;
                }
            }
        }
        if ($directoryCount > self::DIRECTORY_LIMIT) {
            throw new Exception(sprintf('There should be %d directories or less', self::DIRECTORY_LIMIT));
        }
    }

    /**
     * Return iterator for path.
     * @param string $path
     * @return FilesystemIterator
     *
     * @throws UnexpectedValueException
     */
    private function getIterator(string $path): FilesystemIterator
    {
        return new FilesystemIterator($path, FilesystemIterator::SKIP_DOTS|FilesystemIterator::NEW_CURRENT_AND_KEY);
    }

    /**
     * Mark file\directory to delete.
     * @param SplFileInfo $info
     */
    protected function markForDelete(SplFileInfo $info)
    {
        array_push($this->filesToRemove, $info);
    }

    /**
     * Check 2nd level path.
     * @param SplFileInfo $path
     * @return bool If files exist.
     */
    protected function checkPath(SplFileInfo $path): bool
    {
        $filesExist = false;
        $iterator = $this->getIterator($path->getRealPath());

        foreach ($iterator as $file) {
            if ($file->isFile() || $file->isLink()) {
                $this->markForDelete($file);
            }
            if ($file->isDir()) {
                if (!$this->cleanDir($file)) {
                    $this->markForDelete($file);
                } else {
                    $filesExist = true;
                }
            }
        }
        return $filesExist;
    }

    /**
     * Remove all directories and unsupported files.
     * Check 3rd level path.
     * @param SplFileInfo $info
     * @return bool Whether directory contains suitable files.
     */
    protected function cleanDir(SplFileInfo $info): bool
    {
        $cnt = false;
        $iterator = $this->getIterator($info->getRealPath());
        foreach ($iterator as $file) {
            if ($file->isFile()) {
                if (!$this->suitableFile($file)) {
                    $this->markForDelete($file);
                } else {
                    $cnt = true;
                    $this->addSuitableFile($file);
                }
            }
            if ($file->isDir()) {
                $this->markForDelete($file);
            }
        }
        return $cnt;
    }

    /**
     * Check if file is suitable for usage.
     * @param SplFileInfo $info
     * @return bool
     */
    protected function suitableFile(SplFileInfo $info)
    {
        return $info->isFile() && in_array(strtolower($info->getExtension()), $this->exts);
    }

    protected function addSuitableFile(SplFileInfo $file)
    {
        array_push($this->suitableFiles, $file);
    }
}
