<?php
/**
 * @license CC BY-NC 4.0 (http://creativecommons.org/licenses/by-nc/4.0/)
 * @author Rym <rym.the.great@gmail.com>
 */
$archiveName = $argv[1] ?? "grom.phar";
$pack = new packPhar($archiveName);
exit(0);

class packPhar
{
    /**
     * @var \Phar
     */
    protected $phar;

    /**
     * @var array Paths to add into archive
     */
    protected $paths = ['src', 'vendor', 'config'];


    protected $files = ['./composer.json', './composer.lock'];

    /**
     * Add files from provided path into phar.
     * @param string $path Path to add.
     */
    protected function addPath($path)
    {
        $it = new RecursiveIteratorIterator(
            new \fileFilter(
                new RecursiveDirectoryIterator($path)
            )
        );
        $this->phar->buildFromIterator($it, dirname(__FILE__));
    }

    /**
     * Create phar archive.
     * @param string $fname Filename for phar archive.
     */
    public function __construct($fname)
    {
        chdir(dirname(__FILE__));
        $this->phar = new \Phar('build' . DIRECTORY_SEPARATOR . $fname);
        $this->phar->startBuffering();
        $this->setStub();
        foreach ($this->files as $file) {
            $this->phar->addFile($file);
        }

        foreach ($this->paths as $path) {
            $this->addPath($path);
        }
        $this->phar->stopBuffering();
        $this->phar->compressFiles(\Phar::GZ);
    }

    /**
     * Set stub for phar archive.
     */
    protected function setStub()
    {
        $stub = file_get_contents('vendor/laminas/laminas-cli/bin/laminas');
        $stub .= "\n__HALT_COMPILER();\n";
        $this->phar->setStub(trim($stub));
    }
}

/**
 * Class fileFilter
 * Extra class to filter files to be included into phar archive.
 */
class fileFilter extends RecursiveFilterIterator
{
    public function accept(): bool
    {
        $finfo = $this->getInnerIterator()->current();
        if ($finfo->isFile() && ($finfo->getExtension() !== 'php' && !$finfo->isExecutable())) {
            return false;
        }
        return true;
    }
}
