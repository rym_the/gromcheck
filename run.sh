#!/usr/bin/env sh
if [ ! -b "$1" ]
then
  echo "$1 is not a block device"
  exit 1;
fi

MOUNT_POINT=/mnt

mount -o flush,utf8=1 "$1" $MOUNT_POINT

php /app/vendor/bin/laminas grom:cleaner $MOUNT_POINT
/bin/echo 3 > /proc/sys/vm/drop_caches && sleep 10 && umount $MOUNT_POINT
/usr/local/bin/yafs -f "/tmp/yafs.xml" -d $1 -r
php /app/vendor/bin/laminas grom:yafs "/tmp/yafs.xml"
/usr/local/bin/yafs -f "/tmp/yafs.new.xml" -d $1 -w

echo "Done!"
exit 0;
