FROM php:8.2.8-cli-alpine3.18 as base

RUN apk update && \
 apk add dosfstools exfat-utils rsync && \
 apk add ffmpeg flac
ENV LANG ru_RU.UTF-8
ENV LANGUAGE ru_RU:ru
ENV LC_ALL ru_RU.UTF-8
RUN mkdir /tmp/1 && \
 curl -L "https://bitbucket.org/rym_the/yafs/downloads/yafs.alpine.tar.gz" | tar xz -C /tmp/1 && \
 cp -f /tmp/1/* /usr/local/bin && rm -rf /tmp/1

RUN apk add icu-libs icu-data-full

FROM base as dev
COPY --from=composer /usr/bin/composer /usr/bin/composer
RUN apk add --no-cache --virtual .build-deps $PHPIZE_DEPS linux-headers && \
    docker-php-source extract && \
    pecl install xdebug	&& docker-php-ext-enable xdebug && \
    docker-php-source delete && \
    apk del -f .build-deps
WORKDIR "/app"
ENTRYPOINT ["php"]
CMD ["/app/vendor/bin/laminas"]

FROM base as prod
RUN mkdir /app
COPY config/ /app/config/
COPY vendor/ /app/vendor/
COPY src/ /app/src/
COPY run\.sh /app/
COPY composer* /app/

WORKDIR "/app"
ENTRYPOINT ["/app/run.sh"]