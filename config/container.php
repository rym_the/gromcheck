<?php

use Laminas\ServiceManager\ServiceManager;

$config = require __DIR__ . '/autoload/global.config.php';
$container = new ServiceManager($config['service_manager']);
$container->setService('config', $config);
return $container;