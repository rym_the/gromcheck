<?php

use GROM\Commands\Clean;
use GROM\Commands\Rename;
use GROM\Commands\UpdateTag;
use GROM\Commands\Yafs;
use GROM\Models\ModelFactory;
use GROM\Utils\Tools\Tools;
use GROM\Utils\Tools\TrackInfoFactory;
use GROM\Utils\Tools\TrackInfoFlac;
use GROM\Utils\Tools\TrackInfoMp3;
use GROM\Utils\ToolsFactory;
use GROM\Utils\YAFSSorter;
use Laminas\ServiceManager\AbstractFactory\ReflectionBasedAbstractFactory;
use Psr\Container\ContainerInterface;

return [
    'laminas-cli' => [
        'commands' => [
            'grom:yafs' => Yafs::class,
            'grom:cleaner' => Clean::class,
            'grom:rename' => Rename::class,
            'grom:updatetag' => UpdateTag::class,
        ],
        'chains' => [
            Clean::class => [
                UpdateTag::class => ['directory' => 'directory'],
                Rename::class => ['directory' => 'directory'],
            ]
        ]
    ],
    'dependencies' => [],
    'service_manager' => [
        'abstract_factories' => [
        ],
        'factories' => [
            Clean::class => ReflectionBasedAbstractFactory::class,
            Rename::class => ReflectionBasedAbstractFactory::class,
            UpdateTag::class => ReflectionBasedAbstractFactory::class,
            YAFSSorter::class => ReflectionBasedAbstractFactory::class,
            Yafs::class => ReflectionBasedAbstractFactory::class,
            ModelFactory::class => ReflectionBasedAbstractFactory::class,
            TrackInfoMp3::class => static function (ContainerInterface $container): TrackInfoMp3 {
                return new TrackInfoMp3(
                    ToolsFactory::getTool(Tools::FFPROBE),
                    ToolsFactory::getTool(Tools::FFMPEG),
                    ToolsFactory::getTool(Tools::RSYNC),
                    $container->get(ModelFactory::class)
                );
            },
            TrackInfoFlac::class => static function (ContainerInterface $container): TrackInfoFlac {
                return new TrackInfoFlac(
                    ToolsFactory::getTool(Tools::METAFLAC),
                    $container->get(ModelFactory::class)
                );
            },
            ContainerInterface::class => static function (ContainerInterface $container): ContainerInterface {
                return $container;
            },
            TrackInfoFactory::class => ReflectionBasedAbstractFactory::class,
        ],
    ],
];
