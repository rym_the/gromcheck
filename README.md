Script to prepare USB flash to work correctly with ​GROM­USB3 (MAZM8)
=========

Disclaimer
---------
THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### Notice

Please don't use provided script on origin local media. Run on USB flash with copy of original media.

### Tested
* script on Linux system
* USB Flash with media on GROM-USB3(v26)

Script provides functionality:
---------
* rename files in number ordering
* remove empty and extra folders
* remove unsupported files
* transliterate files|tags with russian symbols
* clear flac files and leave only single tags:
    * title
    * album
    * artist
    * track number
    * year of album
* test flac files and re-decode files with problems
* clear mp3 files and leave only tags (in beta):
    * title
    * album
    * artist
    * track number
    * year of album
    * genre
* test mp3 files and re-decode files with problems
* sorts directories and files in YAFS output file

Usage:
----------
* Mount flash drive
* php [grom.phar](../../downloads/grom.phar) run `/path/to/usb/flash`
* sudo umount `/dev/sdx`
* sudo `/path/to/yafs/yafs` -f `/path/to/tmp/yafs.xml` -d `/dev/sdx` -r
* php [grom.phar](../../downloads/grom.phar) yafs `/path/to/tmp/yafs.xml`
* sudo `/path/to/yafs/yafs` -f `/path/to/tmp/yafs.new.xml` -d `/dev/sdx` -w

Docker usage:
----------
* `curl -L "https://bitbucket.org/rym_the/gromcheck/downloads/img.tar.gz" -o img.tar.gz`
* `docker load -i img.tar.gz`
* `docker run --privileged=true -it --tmpfs /tmp grom_img /dev/sda1`

Dev docker usage:
-----------

* `docker build -t grom:latest --target=dev .`
* `docker run --privileged=true -it -u=``id -u``:``id -g`` --tmpfs /tmp  -v=${PWD}:/app grom`

Dependencies:
----------
* [php](https://secure.php.net/)
* [flac](https://xiph.org/flac/)
* [YAFS](http://www.luisrios.eti.br/public/en_us/projects/yafs/)
* [ffmpeg](https://ffmpeg.org/)
* [~~id3v2~~](http://id3v2.sourceforge.net/)
* [~~mp3val~~](http://mp3val.sourceforge.net/)